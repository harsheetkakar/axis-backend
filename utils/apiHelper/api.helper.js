'use strict'
const _ = require('lodash');
var proto = "https";
var host = "sandbox-staging.signzy.tech";
var port = "";
var path = "api/v2";
var endpoints = {
    login: "patrons/login",
    identities: patronId => `patrons/${patronId}/identities`,
    snoops: "snoops",
    hunts: "hunts",
    organizations: patronId => `patrons/${patronId}/organizations`, 
    accountVerify: patronId => `patrons/${patronId}/bankaccountverifications`,
    lpgs: patronId=>`patrons/${patronId}/lpgs`, //changed by ME
    phones: patronId=>`patrons/${patronId}/phones`,
    electricityBills:patronId=>`patrons/${patronId}/electricitybills`,
    facematches:patronId=>`patrons/${patronId}/facematches`,
    gstextractions:patronId=>`patrons/${patronId}/gstextractions`,
    gst:patronId=>`patrons/${patronId}/gstns`,
    epfo:patronId=>`patrons/${patronId}/epfos`,
    udhyogaadhaar:patronId=>`patrons/${patronId}/udyogaadhaars`,
    tin:patronId=>`patrons/${patronId}/tins`,
    vat:patronId=>`patrons/${patronId}/vats`,
    cst:patronId=>`patrons/${patronId}/csts`,
    icai:patronId=>`patrons/${patronId}/icais`,
    icsi:patronId=>`patrons/${patronId}/icsis`,
    mci:patronId=>`patrons/${patronId}/mcis`,
    tan:patronId=>`patrons/${patronId}/tans`,
    fssai :patronId=>`patrons/${patronId}/fssais`,
    iec:patronId=>`patrons/${patronId}/iecs`,
    ptrc:patronId=>`patrons/${patronId}/ptrcs`,
    centralServiceTax:patronId=>`patrons/${patronId}/cservicetaxs`,
    shopAndEstablishment:patronId=>`patrons/${patronId}/snecs`,
    icwai:patronId=>`patrons/${patronId}/icwais`,
    esic:patronId=>`patrons/${patronId}/esics`,
    councilOfArchitecture:patronId=>`patrons/${patronId}/coas`
    // transferVerify: patronId => `/patrons/${patronId}/bankaccountverifications`
}

function generateUrl(proto, host, port, path, endpoint){
    // endpoint is a string, which constructs the endpoint
    if(typeof endpoint === "string"){
        if(port === "")
                return `${proto}://${host}/${path}/${endpoint}`
                // return `${endpoint}`

        else if( parseInt(port)!=NaN // checks for numeric input
                && parseInt(port) >= 0 // unary plus converts to int
                && parseInt(port) < 65536 // valid port numbers are 16bits
        )
            return `${proto}://${host}:${port}/${path}/${endpoint}`
            // return `${endpoint}`
        else
            throw new Error('Invalid port specified. The port must be an integer or numeric string')
    }

    // if endpoint is a function 
    // then call the original funciton
    // and append the result in the place
    else if(typeof endpoint === "function"){
        if(port === '')
            return function(...args){
                return `${proto}://${host}/${path}/${endpoint(...args)}`
            }

        else if( parseInt(port)!=NaN // checks for numeric input
                    && parseInt(port) >= 0 // unary plus converts to int
                    && parseInt(port) <= 0xffff // valid port numbers are 16bits
                )
            return function(...args){
                return `${proto}://${host}:${port}/${path}/${endpoint(...args)}`
            }
        else
            throw new Error('Invalid port specified. The port must be an integer or numeric string')
    }
}

var URLFor = (endpoint) => generateUrl(proto, host,
    port, path, endpoint)

endpoints = _.mapValues(endpoints, URLFor);

module.exports = {
    proto: proto,
    host: host,
    port: port, // leave empty if no port is specified
    path: path,
    endpoints: endpoints,

    URLFor: URLFor


}
