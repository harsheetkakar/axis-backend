'use strict';
const axios = require('axios');
const APIHelper = require('./api.helper.js');
const authCredentials = require("./credentials.json");
const fs = require('fs');
// (function(){
async function sign_in(){
        // console.log(APIHelper.endpoints.login)
        var options = {
            method: 'POST',
            url: APIHelper.endpoints.login,
            headers: {
                accept: '*/*',
                'content-type': 'application/json',
                'accept-language': 'en-US,en;q=0.8'
            },
            data: {
                username: authCredentials.username,
                password: authCredentials.password
            },
            json: true
        };
    
    
        try {
            let response = await axios(options)
            let data = response.data
            let dataToSave = {
                username: authCredentials.username,
                password: authCredentials.password,
                token: {},
                authenticated : true
            }
            dataToSave.token.accessToken = data.id;
            dataToSave.token.ttl = data.ttl;
            dataToSave.token.created = new Date(data.created);
            dataToSave.token.patronId = data.userId;
            dataToSave.token.expires = new Date((+dataToSave.token.created) + dataToSave.token.ttl )

            fs.writeFile("./credentials.json",dataToSave, err => {
                if(err)
                    console.log("Some glitch!",err);
                console.log("Credentials Updated !",dataToSave);
            })

        } catch(error){
            // console.log("Error Occurred")
            throw new Error('sign_in failed\n'+error);
        }
}

(async () => {
    await sign_in();
})();

module.exports = {}
module.exports.sign_in = sign_in;