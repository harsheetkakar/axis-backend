let errorHandler = (app) => {
    app.use(async (ctx, next) => {
        try {
            await next();
            if (ctx.status == 404) throw {
                "error": "We couldn't find what you're looking to, our Scientists're working on it!"
            }
        } catch (err) {
            ctx.status = 400;
            ctx.body = {
                message: 'Woopsie !',
                error: err
            };
        }
    });
}

module.exports = errorHandler;