const Koa = require('koa');
const logger = require('koa-logger');
const bodyParser = require('koa-bodyparser');
const errorHandler = require('./middleware/Errorhandler.middleware');

const app = new Koa();

app.use(cors());
app.use(bodyParser({
    enableTypes: ['text', 'json']
}));

//Error Handler
errorHandler(app);

//Using Koa logger
app.use(logger());

module.exports = app.listen(56001, () =>
    console.log("Server is running in 56001 port !")
);
